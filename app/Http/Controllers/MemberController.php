<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Member;
use PDO;

class MemberController extends Controller
{
    public function index()
    {
        $members = Member::all();

        $colorMap = [
            'Universitas Atma Jaya Yogyakarta' => 'green',
            'Universitas Kristen Duta Wacana' => 'blue',
            'UPN "Veteran" Yogyakarta' => 'red',
        ];

        return view('member/member-index', compact('members', 'colorMap'));
    }

    public function card($id)
    {
        $member = Member::find($id);

        return view('member/member-card', [
            'member' => $member,
            'action' => 'edit'
        ]);
    }
    public function show($id)
    {
        $member = Member::find($id);

        $colorMap = [
            'Universitas Atma Jaya Yogyakarta' => 'green',
            'Universitas Kristen Duta Wacana' => 'blue',
            'UPN "Veteran" Yogyakarta' => 'red',
        ];

        return view('member/member-detail', [
            'member' => $member,
            'colorMap' => $colorMap,
            'action' => 'show'
        ]);
    }

    public function edit($id)
    {
        $member = Member::find($id);

        return view('member/member-detail', [
            'member' => $member,
            'action' => 'edit'
        ]);
    }

    public function update(Request $request, $id)
    {
        $member = Member::find($id);

        $member->name = $request->name;
        $member->university = $request->university;
        $member->from_domicile = $request->from_domicile;
        $member->phone_number = $request->phone_number;

        $member->save();

        return redirect()->intended('/member')->with(['success' => 'Data Member berhasil diubah']);
    }

    public function destroy($id)
    {
        $member = Member::find($id);
        $member->delete();

        return redirect()->intended('/member')->with(['success' => 'Data Member berhasil dihapus']);
    }
}
