<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        $data = [
            ['id' => 1, 'name' => 'Christopher', 'university' => 'Universitas Atma Jaya Yogyakarta', 'location' => 'Yogyakarta', 'phone' => '08123456789'],
            ['id' => 2, 'name' => 'Novianto', 'university' => 'Universitas Atma Jaya Yogyakarta', 'location' => 'Lubuk Linggau', 'phone' => '08123456789'],
            ['id' => 3, 'name' => 'Bagas Adytia', 'university' => 'Universitas Atma Jaya Yogyakarta', 'location' => 'Yogyakarta', 'phone' => '08123456789'],
            ['id' => 4, 'name' => 'Nuel', 'university' => 'Universitas Kristen Duta Wacana', 'location' => 'Tegal', 'phone' => '08123456789'],
            ['id' => 5, 'name' => 'Kiky', 'university' => 'Universitas Kristen Duta Wacana', 'location' => 'Yogyakarta', 'phone' => '08123456789'],
            ['id' => 6, 'name' => 'Edwin', 'university' => 'Universitas Kristen Duta Wacana', 'location' => 'Yogyakarta', 'phone' => '08123456789'],
            ['id' => 7, 'name' => 'Ryzal', 'university' => 'UPN "Veteran" Yogyakarta', 'location' => 'Sumedang', 'phone' => '08123456789'],
            ['id' => 8, 'name' => 'Samuel Kurniago', 'university' => 'Universitas Kristen Duta Wacana', 'location' => 'Purworejo', 'phone' => '08123456789'],
            ['id' => 9, 'name' => 'Fathurrohman', 'university' => 'UPN "Veteran" Yogyakarta', 'location' => 'Yogyakarta', 'phone' => '08123456789'],
            ['id' => 10, 'name' => 'Stevanus', 'university' => 'Universitas Atma Jaya Yogyakarta', 'location' => 'Yogyakarta', 'phone' => '08123456789'],
        ];

        $colorMap = [
            'Universitas Atma Jaya Yogyakarta' => 'green',
            'Universitas Kristen Duta Wacana' => 'blue',
            'UPN "Veteran" Yogyakarta' => 'red',
        ];

        return view('user.user-index', compact('data', 'colorMap'));
    }
}
