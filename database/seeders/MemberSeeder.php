<?php

namespace Database\Seeders;

use App\Models\Member;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MemberSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $members = [
            ['name' => 'Christopher', 'university' => 'Universitas Atma Jaya Yogyakarta', 'from_domicile' => 'Yogyakarta', 'phone_number' => '08123456789'],
            ['name' => 'Novianto', 'university' => 'Universitas Atma Jaya Yogyakarta', 'from_domicile' => 'Lubuk Linggau', 'phone_number' => '08123456789'],
            ['name' => 'Bagas Adytia', 'university' => 'Universitas Atma Jaya Yogyakarta', 'from_domicile' => 'Yogyakarta', 'phone_number' => '08123456789'],
            ['name' => 'Nuel', 'university' => 'Universitas Kristen Duta Wacana', 'from_domicile' => 'Tegal', 'phone_number' => '08123456789'],
            ['name' => 'Kiky', 'university' => 'Universitas Kristen Duta Wacana', 'from_domicile' => 'Yogyakarta', 'phone_number' => '08123456789'],
            ['name' => 'Edwin', 'university' => 'Universitas Kristen Duta Wacana', 'from_domicile' => 'Yogyakarta', 'phone_number' => '08123456789'],
            ['name' => 'Ryzal', 'university' => 'UPN "Veteran" Yogyakarta', 'from_domicile' => 'Sumedang', 'phone_number' => '08123456789'],
            ['name' => 'Samuel Kurniago', 'university' => 'Universitas Kristen Duta Wacana', 'from_domicile' => 'Purworejo', 'phone_number' => '08123456789'],
            ['name' => 'Fathurrohman', 'university' => 'UPN "Veteran" Yogyakarta', 'from_domicile' => 'Yogyakarta', 'phone_number' => '08123456789'],
            ['name' => 'Stevanus', 'university' => 'Universitas Atma Jaya Yogyakarta', 'from_domicile' => 'Yogyakarta', 'phone_number' => '08123456789'],
        ];

        DB::beginTransaction();

        foreach ($members as $member) {
            Member::firstOrCreate($member);
        }

        DB::commit();
    }
}
