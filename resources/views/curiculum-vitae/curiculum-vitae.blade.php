@extends('layout.main')
@section('menu-cv', 'active')
@section('menu-title', 'Curiculum Vitae')
@section('content')
    <div class="container mt-5">
        <div class="row d-flex">
            <div class="col-md-4 d-flex">
                <div class="card flex-fill">
                    <img src="{{ asset('AdminLTE/dist/img/user2-160x160.jpg') }}" class="card-img-top" alt="Profile Image">
                    <div class="card-body">
                        <h5 class="card-title"><strong>Name</strong></h5>
                        <p class="card-text">Robertus Christopher Adipratama</p>
                        <h5 class="card-title"><strong>Email</strong></h5>
                        <p class="card-text">christopher@gmail.com</p>
                        <h5 class="card-title"><strong>Phone</strong></h5>
                        <p class="card-text">+62 123456789</p>
                        <h5 class="card-title"><strong>Address</strong></h5>
                        <p class="card-text">Yogyakarta</p>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="card mb-3">
                    <div class="card-body">
                        <h5 class="card-title"><strong>Ringkasan Profil</strong></h5>
                        <p class="card-text">Saya adalah seorang Developer yang masih ingin belajar</p>
                    </div>
                </div>
                <div class="card mb-3">
                    <div class="card-body">
                        <h5 class="card-title"><strong>Riwayat Pendidikan</strong></h5>
                        <p class="card-text">SMA Mantap Jiwa (2018 - 2020)</p>
                        <p class="card-text">Universitas Atma Jaya Jaya Jaya (2020 - Present)</p>
                    </div>
                </div>
                <div class="card mb-3">
                    <div class="card-body">
                        <h5 class="card-title"><strong>Pengalaman Kerja</strong></h5>
                        <p class="card-text">Software Developer at XYZ Company (2021 - 2022)</p>
                        <p class="card-text">IT Support at ABC Company (2022 - 2023)</p>
                        <p class="card-text">Front End Developer at WOW Company (2023 - Present)</p>
                    </div>
                </div>
                <div class="card mb-3">
                    <div class="card-body">
                        <h5 class="card-title"><strong>Keahlian dan Kualifikasi</strong></h5>
                        <p class="card-text">HTML, CSS, JavaScript, PHP, Laravel</p>
                        <p class="card-text">Microsoft Office</p>
                    </div>
                </div>
                <div class="card mb-3">
                    <div class="card-body">
                        <h5 class="card-title"><strong>Sertifikasi dan Pelatihan</strong></h5>
                        <p class="card-text">Belajar Membuat Aplikasi Android untuk Pemula di Dicoding (2021)</p>
                        <p class="card-text">Belajar Membuat Front-End Web untuk Pemula di Dicoding (2021)</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
