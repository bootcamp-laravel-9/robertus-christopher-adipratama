@extends('layout.main')
@section('menu-member', 'active')
@section('menu-title', 'Bootcamp Member Detail')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="row no-gutters">
                        <div class="col-md-4">
                            <img src="{{ asset('AdminLTE/dist/img/user2-160x160.jpg') }}" class="card-img" alt="Profile Image">
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <h1 class="mb-4"><strong>{{ $member->name }}</strong></h1>
                                <p class="card-text"><strong>University:</strong> {{ $member->university }}</p>
                                <p class="card-text"><strong>From:</strong> {{ $member->from_domicile }}</p>
                                <p class="card-text"><strong>Phone Number:</strong> {{ $member->phone_number }}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <a href="{{ url()->previous() }}" class="btn btn-primary mt-3">Back</a>
            </div>
        </div>
    </div>
@endsection
