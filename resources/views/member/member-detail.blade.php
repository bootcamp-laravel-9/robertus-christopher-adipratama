@extends('layout.main')
@section('menu-member', 'active')
@section('menu-title', $action == 'show' ? 'Bootcamp Member Detail' : 'Edit Bootcamp Member Detail')
@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Member Detail</h3>
        </div>
        <form action="{{ url('/member-update/' . $member->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="card-body">
                <div class="form-group">
                    <label for="exampleInputEmail1">Nama</label>
                    <input name="name" type="text" class="form-control" id="exampleInputEmail1"
                        placeholder="Enter name" value="{{ $member->name }}" {{ $action == 'show' ? 'disabled' : '' }}>
                </div>
                <div class="form-group">
                    <label for="exampleInputUniversity">Asal Kampus</label>
                    <input name="university" type="text" class="form-control" id="exampleInputUniversity"
                        placeholder="Enter university" value="{{ $member->university }}"
                        {{ $action == 'show' ? 'disabled' : '' }}>
                </div>
                <div class="form-group">
                    <label for="exampleInputDomicile">Asal Daerah</label>
                    <input name="from_domicile" type="text" class="form-control" id="exampleInputDomicile"
                        placeholder="Enter domicile" value="{{ $member->from_domicile }}"
                        {{ $action == 'show' ? 'disabled' : '' }}>
                </div>
                <div class="form-group">
                    <label for="exampleInputPhoneNumber">Nomor Telepon</label>
                    <input name="phone_number" type="text" class="form-control" id="exampleInputPhoneNumber"
                        placeholder="Enter phone number" value="{{ $member->phone_number }}"
                        {{ $action == 'show' ? 'disabled' : '' }}>
                </div>
            </div>
            @if ($action != 'show')
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            @endif
        </form>
    </div>
@endsection
