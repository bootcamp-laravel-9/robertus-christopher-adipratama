@extends('layout.main')
@section('menu-member', 'active')
@section('menu-title', 'Bootcamp Member')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Daftar Anggota Bootcamp Technocenter Batch 9</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example2" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Nama</th>
                                    <th>Asal Kampus</th>
                                    <th>Asal Daerah</th>
                                    <th>Nomor Telepon</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($members as $items)
                                    <tr>
                                        <td>{{ $items->id }}</td>
                                        <td>{{ $items->name }}</td>
                                        <td style="color: {{ $colorMap[$items->university] ?? 'black' }}">
                                            {{ $items->university }}</td>
                                        <td>{{ $items->from_domicile }}</td>
                                        <td>{{ $items->phone_number }}</td>
                                        <td>
                                            <a href="{{ url('/member-card/' . $items->id) }}"
                                                class="btn btn-success">Card</a>
                                            <a href="{{ url('/member-detail/' . $items->id) }}"
                                                class="btn btn-primary">Detail</a>
                                            <a href="{{ url('/member-edit/' . $items->id) }}"
                                                class="btn btn-warning">Edit</a>
                                            <form action="{{ url('/member-delete/' . $items->id) }}" method="post"
                                                class="d-inline">
                                                @csrf
                                                @method('delete')
                                                <button type="submit" class="btn btn-danger">
                                                    <i class="fas fa-trash"></i>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
@endsection
