@extends('layout.main')
@section('menu-user', 'active')
@section('menu-title', 'User Static')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Daftar User Static</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example2" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Asal Kampus</th>
                                    <th>Asal Daerah</th>
                                    <th>Nomor Telepon</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $index => $items)
                                    <tr>
                                        <td>{{ $index + 1 }}</td>
                                        <td>{{ $items['name'] }}</td>
                                        <td style="color: {{ $colorMap[$items['university']] ?? 'black' }}">
                                            {{ $items['university'] }}</td>
                                        <td>{{ $items['location'] }}</td>
                                        <td>{{ $items['phone'] }}</td>
                                    </tr>
                                    {{-- 
                                    atau bisa menggunakan if else didalam column

                                    <td>{{ $index + 1 }}</td>
                                    <td>{{ $items['name'] }}</td>
                                    @if ($items['university'] == 'Universitas Atma Jaya Yogyakarta')
                                        <td style="color: green">{{ $items['university'] }}</td>
                                    @elseif($items['university'] == 'Universitas Kristen Duta Wacana')
                                        <td style="color: blue">{{ $items['university'] }}</td>
                                    @elseif($items['university'] == 'UPN "Veteran" Yogyakarta')
                                        <td style="color: red">{{ $items['university'] }}</td>
                                    @endif
                                    <td>{{ $items['location'] }}</td>
                                    <td>{{ $items['phone'] }}</td> 
                                    --}}
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
@endsection
