<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('login', 'AuthController@login');
Route::get('register', 'AuthController@register');
Route::get('dashboard', 'DashboardController@index');
Route::get('user', 'UserController@index');
Route::get('curiculum-vitae', 'CuriculumVitaeController@index');
Route::get('member', 'MemberController@index');
Route::get('member-card/{id}', 'MemberController@card');
Route::get('member-detail/{id}', 'MemberController@show');
Route::get('member-edit/{id}', 'MemberController@edit');
Route::put('member-update/{id}', 'MemberController@update');
Route::delete('member-delete/{id}', 'MemberController@destroy');
